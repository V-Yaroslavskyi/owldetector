#include <thread>
#include <iostream>
#include "windows.h"
#include "Extrapolation.h"
#include "NeuralNet.h"


double Extrapolation_Processing::scaled_knot(double x) 
{
	return (x - knots_min) / (knots_max - knots_min);
}

VectorXd Extrapolation_Processing::scaled_knots(Eigen::VectorXd const &x_vec) 
{
	return x_vec.unaryExpr([this](double x) { return scaled_knot(x); });
}

double Extrapolation_Processing::operator()(double x) 
{
	return round(spline_extr(scaled_knot(x))(0));
}

Extrapolation::Extrapolation(int traj_len, int step, int extrap_depth, TurretContoller *turret_contoller)
{
	this->traj_len = traj_len;
	this->step = step;
	this->extrap_depth = extrap_depth;
	this->turret_controller = turret_contoller;
}

void Extrapolation::set_traj(vector<Coordinate> traj)
{
	if (traj.size() != traj_len)
	{
		throw std::range_error("Trajectory lenth is different with extrapolation class initialization");
	}
	std::thread extrTh(&Extrapolation::make_pred, this, traj);
	extrTh.detach();
}

void Extrapolation::make_pred(vector<Coordinate> traj)
{
	vector<Coordinate> output(this->extrap_depth);
	VectorXd x_vec(this->traj_len);
	VectorXd y_vec(this->traj_len);
	VectorXd time_vec(this->traj_len);
	for (int i = 0; i < this->traj_len; i++)
	{
		x_vec[i] = traj[i].x;
		y_vec[i] = traj[i].y;
		time_vec[i] = traj[i].time;
	}
	Extrapolation_Processing Xt(x_vec, time_vec);
	Extrapolation_Processing Yt(y_vec, time_vec);
	int temp_time = time_vec[this->traj_len - 1];
	for (int i = 0; i < this->extrap_depth; i++)
	{
		temp_time += this->step;
		output[i].time = temp_time;
		output[i].x = Xt(temp_time);
		output[i].y = Yt(temp_time);
	}
	vector<double> tempvecX(3), tempvecY(3);
	tempvecX[0] = turret_controller->position().x;
	tempvecY[0] = turret_controller->position().y;
	for (int i = 0; i < output.size(); i++)
	{
		tempvecX[1] = output[i].x;
		tempvecX[2] = output[i].y;
		tempvecY[1] = output[i].x;
		tempvecY[2] = output[i].y;
		output[i].x = NetX(tempvecX);
		output[i].y = NetY(tempvecY);
	}

	vector<TurretCoordinate> TurrCor;
	for (int i = 0; i < output.size(); i++)
		TurrCor.push_back(TurretCoordinate(output[i].x, output[i].y, output[i].time));

	this->turret_controller->set_new_track(TurrCor);
}

#if 0

for (int i = 0; i < output.size(); i++)
{
	std::cout << output[i].time << "  ";
	std::cout << output[i].x << "  ";
	std::cout << output[i].y << std::endl;
}
#endif // 0


Extrapolation::~Extrapolation(){};