#include <vector>
#include <random>
#include <iostream>
#include <stdexcept>

#include "utils.h"
#include "TurretController.h"
#include "Detector.h"

using utils::uts_now_ms;

int main(int argc, char *argv[])
{
    try
    {   
        std::cout << "Main thread ID: " << std::this_thread::get_id() << std::endl;

        TurretContoller *tc = new TurretContoller();
		std::vector<TurretCoordinate> track1, track2;
        
        // Track 1
        long current_time = uts_now_ms();
        track1.push_back(TurretCoordinate(0, 0, current_time + 13000));
        
        for(int i = 0; i < 200; ++i)
            track1.push_back(TurretCoordinate(i * 5, i * 5, current_time + 13000 + i * 5));
        
        tc->set_new_track(track1);
        
        std::this_thread::sleep_for(milliseconds(20000));
        
        // Track 2
        current_time = uts_now_ms();
        track2.push_back(TurretCoordinate(0, 0, current_time + 13000));
        
        for(int i = 0; i < 200; ++i)
            track2.push_back(TurretCoordinate(i * 3, i * 3, current_time + 13000 + i * 5));
        
        tc->set_new_track(track2);
        
		std::this_thread::sleep_for(milliseconds(5000));
        delete tc;

        system("pause");

        return 0;
    }
    
    catch(std::exception& e)
    {
        std::cerr << "Unhandled exception: " << e.what() << std::endl;
system("pause");
    }
}
