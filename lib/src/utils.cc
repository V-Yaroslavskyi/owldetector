#include <string.h>

#include <string>
#include <vector>
#include <chrono>
#include <sstream>
#include <iostream>
#include <stdexcept>

#include <utils.h>

std::string utils::inttostr(int i)
{
    std::ostringstream ss;
    ss << i;
    return ss.str();
}

int utils::strtoint(const char* str)
{
    if (str == NULL)
        throw std::invalid_argument("Null string is not a valid integer"); 
    
    int i, n, sign;
    i = n = 0;
    sign = (str[0] == '-') ? -1 : 1;
            
    if(str[0] == '-' || str[0] == '+') ++i;
    if(str[i] == '\0') return 1;
        
    for(; str[i] != '\0' && !isspace(str[i]); ++i)
    {
        if(str[i] >= '0' && str[i] <= '9')
            n = 10 * n + (str[i] - '0');
            
        else
            throw std::invalid_argument(std::string("\"") + std::string(str) +
                                        std::string("\" is not a valid integer"));
    }
    
    n *= sign;
    return n;
}

long utils::uts_now_ms()
{
    std::chrono::time_point<std::chrono::high_resolution_clock> uts_now = 
        std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::milliseconds>
        (uts_now.time_since_epoch()).count();
}

std::vector<std::string>& utils::split(const std::string &s, char delim, std::vector<std::string> &elems)
{
    std::stringstream ss(s);
    std::string item;
        
    while (std::getline(ss, item, delim))
    {
        elems.push_back(item);
    }

    return elems;
}
    
std::vector<std::string> utils::split_string(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

std::string utils::format_single_int(const char* format, int arg)
{
    int buff_size = strlen(format) + 64;
    char *buff = new char[buff_size];

    #ifdef _WIN32
    _snprintf(buff, buff_size, format, arg);
    #else
    snprintf(buff, buff_size, format, arg);
    #endif

    std::string buffAsStdStr = buff;
    return buffAsStdStr;
}
