#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <memory>
#include <string>
#include <sstream>
#include <stdexcept>

namespace utils
{
    std::string inttostr(int i);
    int strtoint(const char* str);
    long uts_now_ms();
    std::vector<std::string>& split(const std::string &s, char delim, std::vector<std::string> &elems);
    std::vector<std::string> split_string(const std::string &s, char delim);
    std::string format_single_int(const char* format, int arg);
}
