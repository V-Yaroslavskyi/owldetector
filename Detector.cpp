#include "Detector.h"


void Detector::doWork()
{
	Trajectory trajectory;
	IplImage *img = imAcqGetImg(imAcq);
	Mat grey(img->height, img->width, CV_8UC1);
	cvtColor(cvarrToMat(img), grey, CV_BGR2GRAY);

	tld->detectorCascade->imgWidth = grey.cols;
	tld->detectorCascade->imgHeight = grey.rows;
	tld->detectorCascade->imgWidthStep = grey.step;

	if (showTrajectory)
	{
		trajectory.init(trajectoryLength);
	}

	if (selectManually)
	{

		CvRect box;

		if (getBBFromUser(img, box, gui) == PROGRAM_EXIT)
		{
			return;
		}

		if (initialBB == NULL)
		{
			initialBB = new int[4];
		}

		initialBB[0] = box.x;
		initialBB[1] = box.y;
		initialBB[2] = box.width;
		initialBB[3] = box.height;
	}

	bool reuseFrameOnce = false;
	bool skipProcessingOnce = false;
	if (loadModel && modelPath != NULL)
	{
		tld->readFromFile(modelPath);
		reuseFrameOnce = true;
	}
	
	while (imAcqHasMoreFrames(imAcq))
	{
		double tic = cvGetTickCount();

		if (!reuseFrameOnce)
		{
			cvReleaseImage(&img);
			img = imAcqGetImg(imAcq);

			if (img == NULL)
			{
				printf("current image is NULL, assuming end of input.\n");
				break;
			}

			cvtColor(cvarrToMat(img), grey, CV_BGR2GRAY);
		}

		if (!skipProcessingOnce)
		{
			tld->processImage(cvarrToMat(img));
		}
		else
		{
			skipProcessingOnce = false;
		}


		double toc = (cvGetTickCount() - tic) / cvGetTickFrequency();

		toc = toc / 1000000;

		float fps = 1 / toc;

		int confident = (tld->currConf >= threshold) ? 1 : 0;

		if (showOutput || saveDir != NULL)
		{
			char string[128];

			char learningString[10] = "";

			if (tld->learning)
			{
				strcpy(learningString, "Learning");
			}

			sprintf(string, "#%d,Posterior %.2f; fps: %.2f, #numwindows:%d, %s", imAcq->currentFrame - 1, tld->currConf, fps, tld->detectorCascade->numWindows, learningString);
			CvScalar yellow = CV_RGB(255, 255, 0);
			CvScalar blue = CV_RGB(0, 0, 255);
			CvScalar black = CV_RGB(0, 0, 0);
			CvScalar white = CV_RGB(255, 255, 255);

			if (tld->currBB != NULL)
			{
				CvScalar rectangleColor = (confident) ? blue : yellow;
				cvRectangle(img, tld->currBB->tl(), tld->currBB->br(), rectangleColor, 8, 8, 0);

				if (showTrajectory)
				{
					CvPoint center = cvPoint(tld->currBB->x + tld->currBB->width / 2, tld->currBB->y + tld->currBB->height / 2);
					cvLine(img, cvPoint(center.x - 2, center.y - 2), cvPoint(center.x + 2, center.y + 2), rectangleColor, 2);
					cvLine(img, cvPoint(center.x - 2, center.y + 2), cvPoint(center.x + 2, center.y - 2), rectangleColor, 2);
					trajectory.addPoint(center, rectangleColor);
				}
			}
			else if (showTrajectory)
			{
				trajectory.addPoint(cvPoint(-1, -1), cvScalar(-1, -1, -1));
			}

			if (showTrajectory)
			{
				trajectory.drawTrajectory(img);
			}

			CvFont font;
			cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, .5, .5, 0, 1, 8);
			cvRectangle(img, cvPoint(0, 0), cvPoint(img->width, 50), black, CV_FILLED, 8, 0);
			cvPutText(img, string, cvPoint(25, 25), &font, white);

			if (showForeground)
			{

				for (size_t i = 0; i < tld->detectorCascade->detectionResult->fgList->size(); i++)
				{
					Rect r = tld->detectorCascade->detectionResult->fgList->at(i);
					cvRectangle(img, r.tl(), r.br(), white, 1);
				}

			}


			if (showOutput)
			{
				gui->showImage(img);
				char key = gui->getKey();

				if (key == 'q') break;

				if (key == 'b')
				{

					ForegroundDetector *fg = tld->detectorCascade->foregroundDetector;

					if (fg->bgImg.empty())
					{
						fg->bgImg = grey.clone();
					}
					else
					{
						fg->bgImg.release();
					}
				}

				if (key == 'c')
				{
					//clear everything
					tld->release();
				}

				if (key == 'l')
				{
					tld->learningEnabled = !tld->learningEnabled;
					printf("LearningEnabled: %d\n", tld->learningEnabled);
				}

				if (key == 'a')
				{
					tld->alternating = !tld->alternating;
					printf("alternating: %d\n", tld->alternating);
				}

				if (key == 'e')
				{
					tld->writeToFile(modelExportFile);
				}

				if (key == 'i')
				{
					tld->readFromFile(modelPath);
				}

				if (key == 'r')
				{
					CvRect box;

					if (getBBFromUser(img, box, gui) == PROGRAM_EXIT)
					{
						break;
					}

					Rect r = Rect(box);

					tld->selectObject(grey, &r);
				}
			}

			if (saveDir != NULL)
			{
				char fileName[256];
				sprintf(fileName, "%s/%.5d.png", saveDir, imAcq->currentFrame - 1);

				cvSaveImage(fileName, img);
			}
		}

		if (reuseFrameOnce)
		{
			reuseFrameOnce = false;
		}
	}

	cvReleaseImage(&img);
	img = NULL;

	if (exportModelAfterRun)
	{
		tld->writeToFile(modelExportFile);
	}
}
