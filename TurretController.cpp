#include <map>
#include <vector>
#include <string>
#include <chrono>
#include <thread>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <stdexcept>

#include "serial/serial.h"
#include "utils.h"

#include "TurretController.h"

using std::pair;
using std::vector;
using std::thread;
using std::string;
using std::getline;

using std::iostream;

using std::ifstream;
using std::chrono::milliseconds;
using serial::Serial;
using serial::PortInfo;
using serial::list_ports;
using utils::inttostr;
using utils::strtoint;
using utils::uts_now_ms;
using utils::split_string;
using utils::format_single_int;

TurretPosition::TurretPosition()
{
    this->x = 0;
    this->y = 0;
}

TurretPosition::TurretPosition(int x, int y)
{
    this->x = x;
    this->y = y;
}

TurretCoordinate::TurretCoordinate()
{
    
}

TurretCoordinate::TurretCoordinate(int x, int y, long t)
{
    this->position = TurretPosition(x, y);
    this->t = t;
}

TurretCoordinate::TurretCoordinate(TurretPosition position, long t)
{
    this->position = position;
    this->t = t;
}

TurretContoller::TurretContoller(bool auto_start_background_worker,
                                 int idle_timeout)
{
    // Loading speeds table
    ifstream speeds_table_file;
    speeds_table_file.open(SPEEDS_TABLE_FILE);
    std::string line;
    
    speeds_table = new std::map<int, int>();
    
    while(getline(speeds_table_file, line))
    {
        vector<string> tok = split_string(line, ',');
            
        if(tok.size() != 2)
        {
            std::cout << tok[0] << std::endl;
            throw std::runtime_error("File with speeds is in incorrect format");
        }
            
        (*this->speeds_table)[strtoint(tok[0].c_str())] = strtoint(tok[1].c_str());
    }
    
    speeds_table_file.close();
    
    if(this->speeds_table->size() == 0)
        throw std::runtime_error("File with speeds is empty or is not present");
    
    // Listing all ports
    this->arduino_serial = NULL;
    vector<PortInfo> devices_found = list_ports();
    vector<PortInfo>::iterator iter = devices_found.begin();
    
    // Searching ports for port with Arduino
    while(iter != devices_found.end())
    {
        PortInfo device = *iter++;
                                        
        // If there is "rduino" substring in device.description
        // (skipping first letter as it can be capital or not)
        if(device.description.find("rduino") != string::npos)
        {
            this->arduino_serial = new Serial(device.port, 9600, serial::Timeout::simpleTimeout(1000));
            break;
        }
    }
    
    if(this->arduino_serial == NULL)
        throw std::runtime_error("Port with Arduino was not found");
    
    // Waiting until turret calibrates itself
    // and skipping broken responses
    string first_response;
    do
    {
        this->arduino_serial->write("!T");
        first_response = this->arduino_serial->read(15);
    } while(first_response.length() != 15);
    
    // Clearing buffer
    do
        first_response = this->arduino_serial->read(15);
    while(!first_response.empty());
    
    this->current_max_position = this->get_max_position();
    
    // Now it's time to start a background worker
    // if it is requested
    if(auto_start_background_worker)
        this->start_background_worker(idle_timeout);
}

TurretPosition TurretContoller::get_max_position() const
{
    this->arduino_serial->write("!T");
    string response = this->arduino_serial->read(15);
    
    vector<string> tok = split_string(response, 'Y');
    
    if(tok.size() != 2)
        throw std::runtime_error("I/O error while trying to get max position. " +
                                 string("Got response: \"") + response + "\"");
    
    try
    {
        int x = strtoint(tok[0].c_str() + 3);
        int y = strtoint(tok[1].c_str());
        
        return TurretPosition(x, y);
    }
    
    catch(std::invalid_argument)
    {
        throw std::runtime_error("I/O error while trying to get max position. " +
                                 string("Got response: \"") + response + "\"");
    }
}

void TurretContoller::set_speed(int x, int y) const
{
    this->arduino_serial->write("!S" + format_single_int("%.3i", x) + "X"
                                     + format_single_int("%.3i", y) + "Y");
                                     
}

void TurretContoller::navigate()
{
    while(true)
    {
		if (this->termination_requested)
			break;

        if(this->in_wait)
        {
            std::this_thread::sleep_for(milliseconds(idle_timeout));
            continue;
        }

        long current_time = uts_now_ms();
        
        // Looking for next coordinate with time in future
		while ((this->current_coordinate_index < this->track.size()) &&
			   (this->track[this->current_coordinate_index].t < current_time))
			++(this->current_coordinate_index);

		// If all coordinates are in past - then moving to last one
		if (this->current_coordinate_index >= this->track.size())
			this->current_coordinate_index = this->track.size() - 1;
        
        // If target coordinate changed
        if(this->current_coordinate_index != this->previous_coordinate_index)
        {
            TurretCoordinate next_coordinate = this->track[current_coordinate_index];
            TurretPosition current_position = this->position();
            
            // Calculating approximate speed for smooth movement
            int x_delta = abs(next_coordinate.x() - current_position.x);
            int y_delta = abs(next_coordinate.y() - current_position.y);
            double time_delta = (next_coordinate.t - current_time) / 1000.0;
            
            double x_required_speed = x_delta / time_delta;
            double y_required_speed = y_delta / time_delta;
            
            // Substituting appropriate realtive turret speed
            // (aka 'speed from 1 to 200' or 'speed command argument')
            auto x_speed_index = this->speeds_table->cbegin();
            auto y_speed_index = this->speeds_table->cbegin();
            for(auto i = this->speeds_table->cbegin(); i != this->speeds_table->cend(); ++i)
            {
                if(abs((*i).second - x_required_speed) <
                   abs((*x_speed_index).second - x_required_speed))
                    x_speed_index = i;
                    
                if(abs((*i).second - y_required_speed) <
                   abs((*y_speed_index).second - y_required_speed))
                    y_speed_index = i;
            }
            
            int x_speed = (*x_speed_index).first;
            int y_speed = (*y_speed_index).first;
            
            // Setting speed and initiating movement
            this->set_speed(x_speed, y_speed);
            this->move_cartesian(next_coordinate.x(), next_coordinate.y());
            
            // If current target coordinate is last in sequnce - 
            // then job is done
            if(this->current_coordinate_index >= this->track.size() - 1)
            {
                this->in_wait = true;
                continue;
            }
            
            // Storing previous coordinate index
            this->previous_coordinate_index = this->current_coordinate_index;
        }
    }
}

void TurretContoller::calibrate()
{
    this->arduino_serial->write("!C");
    
    // Waiting until turret calibrates itself
    // and skipping broken responses
    string response;
    do
    {
        this->arduino_serial->write("!T");
        response = this->arduino_serial->read(15); 
    } while(response.length() != 15);
    
    // Clearing buffer
    do
        response = this->arduino_serial->read(15);
    while(!response.empty());
    
    this->current_max_position = this->get_max_position();
}

void TurretContoller::fire() const
{
    this->arduino_serial->write("!F");
}

TurretPosition TurretContoller::position() const
{
    this->arduino_serial->write("!P");
    string response = this->arduino_serial->read(15);    
    
    vector<string> tok = split_string(response, 'Y');

    if(tok.size() != 2)
        throw std::runtime_error("I/O error while trying to get position. " +
                                 string("Got response: \"") + response + "\"");
        
    try
    {
        int x = strtoint(tok[0].c_str() + 3);
        int y = strtoint(tok[1].c_str());
        
        return TurretPosition(x, y);
    }
    
    catch(std::invalid_argument)
    {
        throw std::runtime_error("I/O error while trying to get position");
    }
}

TurretPosition TurretContoller::max_position() const
{
    return this->current_max_position;
}

void TurretContoller::move_cartesian(int x, int y) const
{
    this->arduino_serial->write("!M" + format_single_int("%.5i", x) + "X"
                                     + format_single_int("%.5i", y) + "Y");
}

void TurretContoller::set_new_track(std::vector<TurretCoordinate> track)
{
    //this->terminate_background_worker();
    this->current_coordinate_index = 0;
    this->previous_coordinate_index = -1; // as -1 != 0
    this->track = track;
    //this->start_background_worker(this->idle_timeout);
    this->in_wait = false;
}

void TurretContoller::start_background_worker(int idle_timeout)
{
    this->idle_timeout = idle_timeout;
    this->termination_requested = false;
    this->in_wait = true;
    this->background_worker = new thread(&TurretContoller::navigate, this);
}

void TurretContoller::terminate_background_worker()
{
    this->termination_requested = true;
    background_worker->join();
    delete this->background_worker;
}

TurretContoller::~TurretContoller()
{
    this->terminate_background_worker();
    delete this->arduino_serial;
    delete this->speeds_table;
}
