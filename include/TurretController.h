#include <map>
#include <string>
#include <vector>
#include <thread>
#include <chrono>

#include "serial/serial.h"

#define SPEEDS_TABLE_FILE "speeds-table.csv"
#define DEFAULT_IDLE_TIMEOUT 20

using std::chrono::milliseconds;

using serial::Serial;

class TurretPosition
{
    public: int x, y;
            TurretPosition();
            TurretPosition(int x, int y);
};

class TurretCoordinate
{
    public: TurretPosition position;
            long t;
            TurretCoordinate();
            TurretCoordinate(int x, int y, long t);
            TurretCoordinate(TurretPosition position, long t);
			int x() const { return this->position.x; }
            int y() const { return this->position.y; }
};

class TurretContoller
{
    private: // Private part
             std::map<int, int>* speeds_table;
             Serial* arduino_serial;
             TurretPosition current_max_position;
             TurretPosition get_max_position() const;
             void set_speed(int x, int y) const;
             // Private async part
             int idle_timeout;
             std::vector<TurretCoordinate> track;
             bool in_wait;
             bool termination_requested;
             unsigned current_coordinate_index;
             unsigned previous_coordinate_index;
             void navigate();
             std::thread* background_worker;
    
    public: // Public part
            TurretContoller(bool auto_start_background_worker = true,
                            int idle_timeout = DEFAULT_IDLE_TIMEOUT);
            void calibrate();
            void fire() const;
            TurretPosition position() const;
            TurretPosition max_position() const;
            void move_cartesian(int x, int y) const;
            ~TurretContoller();
            // Public async part
            void start_background_worker(int idle_timeout = DEFAULT_IDLE_TIMEOUT);
            void terminate_background_worker();
            void set_new_track(std::vector<TurretCoordinate> track);
};
