#pragma once

#include "TLD.h"
#include "ImAcq.h"
#include "Gui.h"
#include "Config.h"
#include "TLDUtil.h"
#include "Trajectory.h"

using namespace tld;
using namespace cv;

struct Coordinate
{
	int x, y;
	long int time;
};

class Detector
{
public:
	tld::TLD *tld;
	ImAcq *imAcq;
	tld::Gui *gui;
	bool showOutput;
	bool showTrajectory;
	int trajectoryLength;
	const char *printResults;
	const char *saveDir;
	double threshold;
	bool showForeground;
	bool showNotConfident;
	bool selectManually;
	int *initialBB;
	bool reinit;
	bool exportModelAfterRun;
	bool loadModel;
	const char *modelPath;
	const char *modelExportFile;
	int seed;

	Detector()
	{
		tld = new tld::TLD();
		showOutput = 1;
		printResults = NULL;
		saveDir = ".";
		threshold = 0.5;
		showForeground = 1;

		showTrajectory = true;
		trajectoryLength = 20;
		modelPath = "model";
		loadModel = true;
		selectManually = 0;

		initialBB = NULL;
		showNotConfident = true;

		reinit = 0;

		//loadModel = false;

		exportModelAfterRun = false;
		modelExportFile = "model";
		seed = 0;

		gui = NULL;
		//modelPath = NULL;
		imAcq = NULL;
	}

	~Detector()
	{
		delete tld;
		imAcqFree(imAcq);
	}

	void doWork();
};

