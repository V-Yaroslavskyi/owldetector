#include <unsupported/Eigen/Splines>
#include <vector>
#include "TurretController.h"
#include "Detector.h"

using Eigen::VectorXd;
using Eigen::Spline;
using Eigen::Dynamic;
using Eigen::SplineFitting;

class Extrapolation_Processing
{
public:
	double knots_min;
	double knots_max;
	Spline<double, 1, Dynamic> spline_extr;


	Extrapolation_Processing(VectorXd const &pts, VectorXd const &knots)
		:
		knots_min(knots.minCoeff()),
		knots_max(knots.maxCoeff()),
		spline_extr(SplineFitting<Spline<double, 1, Dynamic>>::Interpolate(pts.transpose(), std::min<int>(knots.rows() - 1, 3), scaled_knots(knots)))
	{
	}
	double scaled_knot(double x);
	VectorXd scaled_knots(Eigen::VectorXd const &x_vec);
	double operator()(double x);

};

class Extrapolation
{
	int traj_len, step, extrap_depth;
	TurretContoller *turret_controller;
public:
	void make_pred(vector<Coordinate> traj);
	Extrapolation(int traj_len, int step, int extrap_depth, TurretContoller *turret_controller);
	~Extrapolation();
	void set_traj(vector<Coordinate> traj);
};