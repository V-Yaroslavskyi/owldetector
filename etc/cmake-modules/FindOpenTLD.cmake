# Module for locating OpenTLD
# WARNING: OpenCV is required for OpenTLD to work
#
# Customizable variables:
#   OPENTLD_ROOT_DIR
#     Specifies the OpenTLD root directory.

SET(OPENTLD_INCLUDE_DIRS
    ${OPENTLD_ROOT_DIR}/src/3rdparty/cvblobs
    ${OPENTLD_ROOT_DIR}/src/3rdparty/libconfig
    ${OPENTLD_ROOT_DIR}/src/libopentld/tld
    ${OPENTLD_ROOT_DIR}/src/libopentld/mftracker
    ${OPENTLD_ROOT_DIR}/src/libopentld/imacq
    ${OPENTLD_ROOT_DIR}/src/OPENTLD/main)

INCLUDE_DIRECTORIES(${OPENTLD_INCLUDE_DIRS})

IF (NOT EXISTS ${OPENTLD_ROOT_DIR}/build/lib)
    MESSAGE ("Error: Built OpenTLD build dir was not found")
    RETURN ()
ENDIF ()

SET (OPENTLD_BUILT_LIBS_ROOT ${OPENTLD_ROOT_DIR}/build/lib)

IF (EXISTS ${OPENTLD_BUILT_LIBS_ROOT}/Release/)
    SET (OPENTLD_BUILT_LIBS_ROOT ${OPENTLD_BUILT_LIBS_ROOT}/Release)
ELSEIF (EXISTS ${OPENTLD_BUILT_LIBS_ROOT}/Debug/)
    SET (OPENTLD_BUILT_LIBS_ROOT ${OPENTLD_BUILT_LIBS_ROOT}/Debug)
ENDIF ()

SET (OpenTLD_LIBS config++ cvblobs opentld)
LINK_DIRECTORIES (${OPENTLD_BUILT_LIBS_ROOT})