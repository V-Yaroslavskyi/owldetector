#include "Extrapolation.h"
#include "Detector.h"
#include <vector>
#include <cmath>
#include <stdexcept>
#include "NeuralNet.h"
#include "utils.h"

int main()
{	
	TurretContoller *TurCon = new TurretContoller();
	Extrapolation extr(3, 2000, 4, TurCon);
	vector<Coordinate> traj(3);
	Coordinate c1, c2, c3;
	c1.x = 100;
	c2.x = 125;
	c3.x = 138;
	c1.y = 100;
	c2.y = 143;
	c3.y = 168;
	c1.time = utils::uts_now_ms() + 20000;
	c2.time = utils::uts_now_ms() + 22000;
	c3.time = utils::uts_now_ms() + 24000;
	traj[0] = c1;
	traj[1] = c2;
	traj[2] = c3;
	//extr.set_traj(traj);
	extr.make_pred(traj);
	std::this_thread::sleep_for(milliseconds(60000));
	delete TurCon;
	system("pause");
}

